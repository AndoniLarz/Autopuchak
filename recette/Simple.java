import tec.Usager;
import tec.Transport;
import static tec.Fabrique.* ; //importe toutes les méthodes de Fabrique

class Simple {

  /*
   * Affiche les etats d'un usager et d'un transport.
   * Sur un parametre de type Object, la methode println()
   * utilise la methode toString().
   * La methodes toString() doit etre redefinie dans les
   * deux classes PassagerStandard et Autobus.
   */
  static private void deboguerEtat (Transport t, Usager p) {
    System.out.println(p);
    System.out.println(t);
  }

  static public void main (String[] args) {
    // Vehicule serenity = new Autobus(1, 2);
    // Transport laNavette = creerNavette(4,6,7);
    Transport basqueBondissant = creerAutobus(4, 6);

    Usager kaylee = creerPassagerStandard("Kaylee", 4);
    Usager jayne = creerPassagerStandard("Jayne", 4);
    Usager inara = creerPassagerStandard("Inara", 5);
    Usager andouni = creerPassagerLunatique("Andouni", 5);
    Usager puch = creerPassagerStresse("Puch", 6);

    System.out.println(basqueBondissant);

    basqueBondissant.allerArretSuivant();
    //1
    kaylee.monterDans(basqueBondissant);
    puch.monterDans(basqueBondissant);

    System.out.println(basqueBondissant);
    System.out.println(kaylee);
    System.out.println(puch);

    basqueBondissant.allerArretSuivant();
    //2
    andouni.monterDans(basqueBondissant);
    jayne.monterDans(basqueBondissant);

    System.out.println(basqueBondissant);
    System.out.println(kaylee);
    System.out.println(jayne);
    System.out.println(puch);
    System.out.println(andouni);

    basqueBondissant.allerArretSuivant();
    //3
    inara.monterDans(basqueBondissant);

    System.out.println(basqueBondissant);
    System.out.println(kaylee);
    System.out.println(jayne);
    System.out.println(inara);
    System.out.println(puch);
    System.out.println(andouni);

    basqueBondissant.allerArretSuivant();
    //4
    System.out.println(basqueBondissant);
    System.out.println(kaylee);
    System.out.println(jayne);
    System.out.println(inara);
    System.out.println(puch);
    System.out.println(andouni);

    basqueBondissant.allerArretSuivant();
    //5
    System.out.println(basqueBondissant);
    System.out.println(kaylee);
    System.out.println(jayne);
    System.out.println(inara);
    System.out.println(puch);
    System.out.println(andouni);

    basqueBondissant.allerArretSuivant();
    //6
    System.out.println(basqueBondissant);
    System.out.println(kaylee);
    System.out.println(jayne);
    System.out.println(inara);
    System.out.println(puch);
    System.out.println(andouni);

    basqueBondissant.allerArretSuivant();
    //7 tout le monde descend
    System.out.println(basqueBondissant);
    System.out.println(kaylee);
    System.out.println(jayne);
    System.out.println(inara);
    System.out.println(puch);
    System.out.println(andouni);



  }
}

/* Resultat de l'execution.
[arret :0, assis:<0 [0,4[>, debout:<0 [0,6[>]
[arret :1, assis:<1 [0,4[>, debout:<1 [0,6[>]
Kaylee <debout>
Puch <assis>
[arret :2, assis:<2 [0,4[>, debout:<2 [0,6[>]
Kaylee <assis>
Jayne <debout>
Puch <assis>
Andouni <debout>
[arret :3, assis:<1 [0,4[>, debout:<2 [0,6[>]
Kaylee <endehors>
Jayne <endehors>
Inara <debout>
Puch <debout>
Andouni <assis>
[arret :4, assis:<0 [0,4[>, debout:<2 [0,6[>]
Kaylee <endehors>
Jayne <endehors>
Inara <endehors>
Puch <debout>
Andouni <debout>
[arret :5, assis:<0 [0,4[>, debout:<1 [0,6[>]
Kaylee <endehors>
Jayne <endehors>
Inara <endehors>
Puch <debout>
Andouni <endehors>
[arret :6, assis:<0 [0,4[>, debout:<0 [0,6[>]
Kaylee <endehors>
Jayne <endehors>
Inara <endehors>
Puch <endehors>
Andouni <endehors>
[arret :7, assis:<0 [0,4[>, debout:<0 [0,6[>]
Kaylee <endehors>
Jayne <endehors>
Inara <endehors>
Puch <endehors>
Andouni <endehors>

*/
