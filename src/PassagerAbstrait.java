package tec;

public abstract class PassagerAbstrait implements Passager, Usager {

  private String nom ;
  private Position position ;
  private int destination ;

  // On ne peut pas créer de PassagerAbstrait car une classe abstraite ne s'instancie pas.
  // static public Usager creerPassagerAbstrait(String nom, int destination){
  //   return new PassagerStandard(nom, destination);
  // }

  protected PassagerAbstrait(String nom, int destination){
    this.nom = nom ;
    this.position = Position.creerPosition();
    this.destination = destination ;
  }

  public void changerEnAssis(){
    this.position = this.position.assis();
  }

  public void changerEnDebout(){
    this.position = this.position.debout();
  }

  public void changerEnDehors(){
    this.position = this.position.dehors();
  }

  public boolean estAssis(){
    return this.position.estAssis();
  }

  public boolean estDebout(){
    return this.position.estDebout();
  }

  public boolean estDehors(){
    return this.position.estDehors();
  }

  public String nom(){
    return this.nom;
  }

  protected int getDestination(){
    return this.destination;
  }

  // Méthode permettant de factoriser le code minimal de la transformation présent dans toutes les méthodes monterDans
  final public void monterDans(Transport unTransport) {
    // On transtype pour pouvoir utiliser les méthodes de VehiculeMontee, qui ne sont pas dans Transport
    VehiculeMontee unVehiculeMontee = (VehiculeMontee) unTransport ;
    // this.faireMonterDans() permet d'utiliser le comportement selon la classe qui l'exécute
    choixPlaceMontee(unVehiculeMontee);
  }

  // Méthode abstraite à redéfinir dans les classes filles
  protected abstract void choixPlaceMontee(VehiculeMontee unVehiculeMontee);

  final public void nouvelArret(VehiculeArret unVehiculeArret, int numeroArret){
    if (this.destination == numeroArret)
        unVehiculeArret.arretDemanderSortie(this);
    else
      choixChangerPlace(unVehiculeArret, numeroArret, destination);
  }

  // Méthode abstraite à redéfinir dans les classes filles
  protected abstract void choixChangerPlace(VehiculeArret unVehiculeArret, int numeroArret, int destination);


  public String toString(){
    return "" + this.nom + " " + this.position.toString(); // + " Destination : " + this.destination ;
  }
}
