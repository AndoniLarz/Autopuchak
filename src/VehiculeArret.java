package tec;

interface VehiculeArret {

  // METHODES, publiques obligatoirement étant donné que c'est une interface

  public boolean aPlaceAssise();
  public boolean aPlaceDebout();

  void arretDemanderDebout(Passager p);
  void arretDemanderAssis(Passager p);
  void arretDemanderSortie(Passager p);

}
