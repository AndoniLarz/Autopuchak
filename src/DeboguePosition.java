package tec;

public class DeboguePosition {

  public static void main (String[] args) {

    Position maPosition = Position.creerPosition();

    for (int i = 0;i < 4 ; i++ ) {

      switch (i) {
        case 1 :
          maPosition = maPosition.debout();
          System.out.println("On passe à debout");
          break;
        case 2 :
          maPosition = maPosition.assis();
          System.out.println("On passe à assis");
          break;
        case 3 :
          maPosition = maPosition.dehors();
          System.out.println("On passe dehors");
      }

      if (maPosition.estAssis()) {
          System.out.println("assis == true");
      }
      else {
        System.out.println("assis == false");
      }
      if (maPosition.estDebout()) {
        System.out.println("debout == true");
      }
      else {
        System.out.println("debout == false");
      }
      if (maPosition.estDehors()) {
        System.out.println("Position est dehors");
      }
      if (maPosition.estInterieur()) {
        System.out.println("Position est à l'intérieur");
      }

      System.out.println(maPosition.toString());
    }
  }
}
