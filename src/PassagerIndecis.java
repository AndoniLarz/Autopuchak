package tec;

public class PassagerIndecis implements Passager, Usager {

  private String nom ;
  private Position position ;
  private int destination ;

  static public Usager creerPassagerIndecis(String nom, int destination){
    return new PassagerIndecis(nom, destination);
  }

  private PassagerIndecis(String nom, int destination){
    this.nom = nom ;
    this.position = Position.creerPosition();
    this.destination = destination ;
  }

  public void changerEnAssis(){
    this.position = this.position.assis();
  }

  public void changerEnDebout(){
    this.position = this.position.debout();
  }

  public void changerEnDehors(){
    this.position = this.position.dehors();
  }

  public boolean estAssis(){
    return this.position.estAssis();
  }

  public boolean estDebout(){
    return this.position.estDebout();
  }

  public boolean estDehors(){
    return this.position.estDehors();
  }

  public void monterDans(Transport unTransport){
    VehiculeMontee unVehiculeMontee = (VehiculeMontee) unTransport;
    if (unVehiculeMontee.aPlaceDebout())
      unVehiculeMontee.monteeDemanderDebout(this);
  }

  public String nom(){
    return this.nom;
  }

  public void nouvelArret(VehiculeArret unVehiculeArret, int numeroArret){
    if (this.destination == numeroArret) {
      unVehiculeArret.arretDemanderSortie(this);
    }
    else {
      // On demande à changer de place à chaque arrêt
      if (this.position.estDebout())
        unVehiculeArret.arretDemanderAssis(this);
      else
        unVehiculeArret.arretDemanderDebout(this);
    }
  }

  public String toString(){
    return "" + this.nom + " " + this.position.toString(); // + " Destination : " + this.destination ;
  }
}
