package tec;

class PassagerLunatique extends PassagerAbstrait {

  static public Usager creerPassagerLunatique(String nom, int destination){
    return new PassagerLunatique(nom, destination);
  }

  private PassagerLunatique(String nom, int destination){
    super(nom, destination);
  }

  public void choixPlaceMontee(VehiculeMontee unVehiculeMontee){
     if (unVehiculeMontee.aPlaceDebout())
      unVehiculeMontee.monteeDemanderDebout(this);
  }

  public void choixChangerPlace(VehiculeArret unVehiculeArret, int numeroArret, int destination){
    if (destination == numeroArret)
      unVehiculeArret.arretDemanderSortie(this);
    else {
      if (this.estDebout())
        unVehiculeArret.arretDemanderAssis(this);
      else
        unVehiculeArret.arretDemanderDebout(this);
    }
  }

}
