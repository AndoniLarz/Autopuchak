package tec;

class Autobus implements VehiculeArret, VehiculeMontee, Transport {

	// ATTRIBUTS

	private int arret;
	private Passager[] tabPassagers;

	private Jauge jaugyAssis;
	private Jauge jaugyDebout;

	// METHODES


	static public Transport creerAutobus(int nbPlaceAssise, int nbPlaceDebout){
		return new Autobus(nbPlaceAssise, nbPlaceDebout);
	}


	private Autobus(int nbPlaceAssise, int nbPlaceDebout) {
		arret = 0; //initialisation du numéro d'arrêt de l'autobus
	  tabPassagers = new Passager[nbPlaceAssise + nbPlaceDebout]; //tableau de passagers
	  jaugyAssis = new Jauge(nbPlaceAssise, 0); //jauge de disponibilité du nombre de places assises
	  jaugyDebout = new Jauge(nbPlaceDebout, 0); //jauge de disponibilité du nombre de places debouts
  }

  public void allerArretSuivant(){
  	arret++;
  	for (int i = 0; i < tabPassagers.length ; i++) {
  		if (tabPassagers[i] != null) {
  			tabPassagers[i].nouvelArret(this, arret);
  		}
  	}
  }

  public boolean aPlaceAssise(){
  	return this.jaugyAssis.estVert();
  }

	public boolean aPlaceDebout(){
		return this.jaugyDebout.estVert();
	}

	// Méthodes quand le bus s'arrête

	public void arretDemanderAssis(Passager p){
		if (this.aPlaceAssise() && p.estDebout()) {
			p.changerEnAssis();
			this.jaugyAssis.incrementer();
			this.jaugyDebout.decrementer();
		}
	}

	public void arretDemanderDebout(Passager p){
		if (this.aPlaceDebout() && p.estAssis()) {
			p.changerEnDebout();
			this.jaugyDebout.incrementer();
			this.jaugyAssis.decrementer();
		}
	}

	public void arretDemanderSortie(Passager p){
		if (!p.estDehors()) {
			if (this.supprimerPassager(p) >= 0) {
				if(p.estAssis())
					this.jaugyAssis.decrementer();
				else
					this.jaugyDebout.decrementer();
				p.changerEnDehors();
			}
		}
	}

	// Méthodes quand un passager monte

	public void monteeDemanderAssis(Passager p){
		if (this.aPlaceAssise()){
			if (this.ajouterPassager(p) < 0)
				return;
			else {
				jaugyAssis.incrementer();
				p.changerEnAssis();
			}
		}
	}

	public void monteeDemanderDebout(Passager p){
		if (this.aPlaceDebout()){
			if (this.ajouterPassager(p) < 0)
				return;
			else {
				jaugyDebout.incrementer();
				p.changerEnDebout();
			}
		}
	}

	public String toString(){
		return "[arret :" + arret + ", assis:" + jaugyAssis.toString() + ", debout:" + jaugyDebout.toString() + "]";
	}



	// METHODES PRIVEES INTERNES

	private int ajouterPassager(Passager p) {
		for (int i = 0; i < tabPassagers.length; i++){
  		if (null == tabPassagers[i]) {
    		tabPassagers[i] = p;
    		return i;
  		}
		}
		return -1;
	}

	private int supprimerPassager(Passager p) {
		for (int i = 0; i < tabPassagers.length; i++){
  		if (p == tabPassagers[i]){
				tabPassagers[i] = null ;
				return i;
			}
		}
		return -1;
	}
}
