package tec;

class PassagerStandard extends PassagerAbstrait {

  static public Usager creerPassagerStandard(String nom, int destination){
    return new PassagerStandard(nom, destination);
  }

  private PassagerStandard(String nom, int destination){
    super(nom, destination);
  }

  public void choixPlaceMontee(VehiculeMontee unVehiculeMontee){
     if (unVehiculeMontee.aPlaceDebout())
      unVehiculeMontee.monteeDemanderDebout(this);
  }

  public void choixChangerPlace(VehiculeArret unVehiculeArret, int numeroArret, int destination){
    if (destination == numeroArret+1)
      unVehiculeArret.arretDemanderSortie(this);
    else {
      // On demande à être assis à chaque arrêt
      if (this.estDebout())
        unVehiculeArret.arretDemanderAssis(this);
    }
  }

}
