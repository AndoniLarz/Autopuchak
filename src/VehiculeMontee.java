package tec;

interface VehiculeMontee {

  // METHODES, publiques obligatoirement étant donné que c'est une interface

  public boolean aPlaceAssise();
  public boolean aPlaceDebout();

  public void monteeDemanderAssis(Passager p);
  public void monteeDemanderDebout(Passager p);

}
