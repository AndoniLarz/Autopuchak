package tec;

public class Fabrique {

  // On évite ainsi de pouvoir instancier la Fabrique
  private Fabrique(){}

  static public Transport creerAutobus(int nbPlaceAssise, int nbPlaceDebout){
    return Autobus.creerAutobus(nbPlaceAssise, nbPlaceDebout);
  }

  static public Transport creerNavette(int nbPlaceAssise, int nbPlaceDebout, int nbArrets) {
    return NavetteTerminus.creerNavette(nbPlaceAssise, nbPlaceDebout, nbArrets);
  }

  static public Usager creerPassagerStandard(String nom, int destination){
    return PassagerStandard.creerPassagerStandard(nom, destination);
  }

  static public Usager creerPassagerIndecis(String nom, int destination){
    return PassagerIndecis.creerPassagerIndecis(nom, destination);
  }

  static public Usager creerPassagerLunatique(String nom, int destination){
    return PassagerLunatique.creerPassagerLunatique(nom, destination);
  }

  static public Usager creerPassagerStresse(String nom, int destination){
    return PassagerStresse.creerPassagerStresse(nom, destination);
  }
}
