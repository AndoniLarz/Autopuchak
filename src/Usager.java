package tec;

public interface Usager {

  // METHODES

  public void monterDans(Transport b);
  public String nom();
  public boolean estDehors();
  public boolean estAssis();
  public boolean estDebout();
}