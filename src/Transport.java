package tec;

public interface Transport {

  // METHODES, publiques obligatoirement étant donné que c'est une interface

  public void allerArretSuivant();

  public boolean aPlaceAssise();
  public boolean aPlaceDebout();

}
