package tec;

interface Passager {

  String nom();

  boolean estDehors();
  boolean estAssis();
  boolean estDebout();

  void changerEnDehors();
  void changerEnAssis();
  void changerEnDebout();

  void nouvelArret(VehiculeArret b, int numeroArret);
}
