package tec;

public class NavetteTerminus implements VehiculeArret, VehiculeMontee, Transport {

	// ATTRIBUTS

	private int arret;
	private int terminus;
	private Passager[] tabPassagers;

	private Jauge jaugyAssis;
	private Jauge jaugyDebout;

	// METHODES

	static public Transport creerNavette(int nbPlaceAssise, int nbPlaceDebout, int nbArrets){
		return new NavetteTerminus(nbPlaceAssise, nbPlaceDebout, nbArrets);
	}


	private NavetteTerminus(int nbPlaceAssise, int nbPlaceDebout, int nbArrets) {
		arret = 0; //initialisation du numéro d'arrêt de l'autobus
		this.terminus = nbArrets; // Initialisation du terminus
	  tabPassagers = new Passager[nbPlaceAssise + nbPlaceDebout]; //tableau de passagers
	  jaugyAssis = new Jauge(nbPlaceAssise, 0); //jauge de disponibilité du nombre de places assises
	  jaugyDebout = new Jauge(nbPlaceDebout, 0); //jauge de disponibilité du nombre de places debouts
  	}

  	public void allerArretSuivant(){
  		if (arret < terminus) {
  			arret++;
		}
		if (arret == terminus) {
			for (int i = 0; i < tabPassagers.length ; i++) {
  				if (tabPassagers[i] != null) {
  					if (tabPassagers[i].estAssis()) {
  						jaugyAssis.decrementer();
  					}
  					else if (tabPassagers[i].estDebout()) {
  						jaugyDebout.decrementer();
  					}
  					tabPassagers[i].changerEnDehors();
  					tabPassagers[i] = null ;
  				}
  			}
  		}
  	}

  	public boolean aPlaceAssise(){
  		return this.jaugyAssis.estVert();
  	}

	public boolean aPlaceDebout(){
		return this.jaugyDebout.estVert();
	}

	// Méthodes quand le bus s'arrête

	public void arretDemanderAssis(Passager p){}

	public void arretDemanderDebout(Passager p){}

	public void arretDemanderSortie(Passager p){}

	// Méthodes quand un passager monte

	public void monteeDemanderAssis(Passager p){
		if (this.aPlaceAssise()){
			if (this.ajouterPassager(p) < 0)
				return;
			else {
				jaugyAssis.incrementer();
				p.changerEnAssis();
			}
		}
	}

	public void monteeDemanderDebout(Passager p){
		if (this.aPlaceDebout()){
			if (this.ajouterPassager(p) < 0)
				return;
			else {
				jaugyDebout.incrementer();
				p.changerEnDebout();
			}
		}
	}

	public String toString(){
		return "[arret :" + arret + ", terminus:" + terminus + ", assis:" + jaugyAssis.toString() + ", debout:" + jaugyDebout.toString() + "]";
	}



	// METHODES PRIVEES INTERNES

	private int ajouterPassager(Passager p) {
		for (int i = 0; i < tabPassagers.length; i++){
  		if (null == tabPassagers[i]) {
    		tabPassagers[i] = p;
    		return i;
  		}
		}
		return -1;
	}

	// Commentée car inutile pour cette classe.

	// private int supprimerPassager(Passager p) {
	// 	for (int i = 0; i < tabPassagers.length; i++){
 //  		if (p == tabPassagers[i]){
	// 			tabPassagers[i] = null ;
	// 			return i;
	// 		}
	// 	}
	// 	return -1;
	// }
}
