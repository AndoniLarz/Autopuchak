package tec;
/**
 * Cette classe represente la position d'un passager par rapport a un transport.
 * Une position a trois etats possibles : assis dans un transport,
 * debout dans un transport et dehors d'un transport.
 *
 * Les instances de cette classe sont des objets constants.
 *
 * @author Georgy
 * @since 2007-2016
 **/
class Position {



  final private static  Position posDehors = new Position();
  final private static  Position posAssis = new Position();
  final private static  Position posDebout = new Position();


  private Position(){}
  

  static public Position creerPosition(){
    return posDehors;
  }
  

  /**
   * construit une l'instance dans la position dehors.
   *
   */
  // public Position() {
  //   courant = DEHORS;
  // }

  /**
   * Construit une instance en precisant un des positions du passager.
   *
   * @param e valeur de l'etat.
   */
  // private Position(int e) {
  //   courant = e;
  // }

  /**
   * La position est-elle dehors ?
   *
   * @return vrai si l'etat de l'instance est dehors;
   */
  public boolean estDehors() {
    return this == posDehors;
  }

  /**
   * La position est-elle assis ?
   *
   * @return vrai si l'etat de l'instance est assis;
   */
  public boolean estAssis() {
    return this == posAssis;
  }

  /**
   * La position est-elle debout ?
   *
   * @return vrai si l'etat de l'instance est debout;
   */
  public boolean estDebout() {
    return this == posDebout;
  }

  /**
   * La position est-elle assis ?
   *
   * @return vrai la position est assis ou debout.
   */
  public boolean estInterieur() {
    return !(this == posDehors);
  }


  /**
   * Fournit une position assis.
   *
   * @return instance dans l'etat assis.
   */
  public Position assis() {
    return this.posAssis;
  }

  /**
   * Fournit une position debout.
   *
   * @return instance dans l'etat debout.
   */
  public Position debout() {
    return this.posDebout;
  }

  /**
   * Fournit une position dehors.
   *
   * @return instance dans l'etat dehors.
   */
  public Position dehors() {
    return this.posDehors;
  }

  /**
   * Cette methode est heritee de la classe {@link java.lang.Object}.
   * Tres utile pour le debogage, elle permet de fournir une
   * chaine de caracteres correspondant a l'etat d'un objet.
   * Mais, il faut adapter le code de cette methode a chaque classe.
   *
   * @return une des chaines "<endehors>", "<assis>", "<debout>"
   * en fonction de la position courante.
   */
  @Override
  public String toString() {
    String nom = null;
    if(this == posDehors) 
      nom = "endehors";
    else if(this == posAssis)
      nom = "assis";
    else
      nom = "debout";
      
    return "<" + nom + ">";
  }
}
