package tec;

class PassagerStresse extends PassagerAbstrait {

    static public Usager creerPassagerStresse(String nom,int destination){
        return new PassagerStresse(nom, destination);
    }

    private PassagerStresse(String nom, int destination){
        super(nom, destination);
  }

  public void choixPlaceMontee(VehiculeMontee unVehiculeMontee){
    if (unVehiculeMontee.aPlaceAssise())
      unVehiculeMontee.monteeDemanderAssis(this);
  }

  public void choixChangerPlace(VehiculeArret unVehiculeArret, int numeroArret, int destination){
    if (destination == numeroArret)
        unVehiculeArret.arretDemanderSortie(this);
    else if((destination <= numeroArret + 3) && !(this.estDebout()))
        unVehiculeArret.arretDemanderDebout(this);
  }
}
