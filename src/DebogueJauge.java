package tec;

public class DebogueJauge{

	public static void main (String[] args) {

	    Jauge maPetiteJauge = new Jauge(20, 5);

	    int firstArg;
		if (args.length > 0) {
		    try {
		        firstArg = Integer.parseInt(args[0]);
		        System.out.println("Réservation de " + firstArg + " places.");
		        for (int i = 0; i < firstArg ; i++ ) {
		        	maPetiteJauge.incrementer();
		        }
		        if (maPetiteJauge.estRouge())
				      System.out.println("c est rouge");
				if (maPetiteJauge.estVert())
				      System.out.println("c est vert");

			    System.out.println(maPetiteJauge);

		    } catch (NumberFormatException e) {
		        System.err.println("Argument" + args[0] + " must be an integer.");
		        System.exit(1);
		    }
		}
  }
}
